#!/bin/bash

set -o errexit

bamFile=$1

sampleID=$(basename ${bamFile/\.bam/})

if [ -f PindelVCFBROCA/$sampleID.vcf ]
then
  echo Already processed $sampleID
  exit 0
fi

refGenomeFile=/Data/Genomes/GENCODE_19/GRCh37.p13.genome.fa

outInsertSizeFile=InsertSizeMetrics/$sampleID
pairedStatus=$(cat PairedStatus/$sampleID)

if [ ! -f $outInsertSizeFile ]
then
  java -Xmx16g -jar /Software/picard-tools-1.131/picard.jar CollectInsertSizeMetrics INPUT=$bamFile OUTPUT=$outInsertSizeFile HISTOGRAM_FILE=/dev/null VALIDATION_STRINGENCY=SILENT
fi

if [ "$pairedStatus" == "single" ]
then
  echo Cannot apply pindel to single-end reads
  exit 0
fi

tmpDir=/tmp/$sampleID
configFile=$tmpDir/pindel.cfg

#rm -rfv $tmpDir
#mkdir -pv $tmpDir/VCF

function cleanup {
  rm -rfv $tmpDir
}

#trap 'cleanup' TERM INT EXIT

insertSize=$(python code/GetAverageInsertSize.py InsertSizeMetrics/$sampleID)

echo -e "$bamFile $insertSize $sampleID" >> $configFile

export PATH="$PATH:/Software/pindel/"

#for i in {1..22} X
#do
#  outDir=$tmpDir/chr${i}
#  rm -rf $outDir
#  mkdir -p $outDir
#
#  pindel -f $refGenomeFile -i $configFile -c chr${i} -o $outDir -w 50 -t 1 --NormalSamples true --report_interchromosomal_events false &
#done
#wait

#echo Converting pindel files to vcf
#for i in {1..22} X
#do
#  outDir=$tmpDir/chr${i}
#
#  echo $i
#  pindel2vcf -r $refGenomeFile -R "GENCODE_GRCh37.p13" -d "July 2013" -c chr${i} -P $outDir -w 50 -v $tmpDir/VCF/chr${i}.vcf &
#done
#wait

#echo Checking for empty vcf files and excluding them
#for f in $tmpDir/VCF/*.vcf
#do
#  python /Code/PrintLinesNotStartingWith.py $f "#" > $f.tmp
#  numLines=$(wc -l $f.tmp)
#
#  if [ "$numLines" == "0 $f.tmp" ]
#  then
#    rm -fv $f
#  fi
#
#  rm -fv $f.tmp
#done

#echo bgzipping and tabixing files
#for f in $tmpDir/VCF/*.vcf
#do
#  bgzip $f
#  tabix $f.gz
#done

#echo Identifying files to concatenate
#vcfFilesText=
#for i in {1..22} X
#do
#  f=$tmpDir/VCF/chr${i}.vcf.gz
#
#  if [ -f $f ]
#  then
#    vcfFilesText="$vcfFilesText $f"
#  fi
#done

export PATH="$PATH:/Software/bcftools"

#echo Concatenating files
#bcftools concat -a -D -o PindelVCF/$sampleID.vcf $vcfFilesText

echo Selecting BROCA variants
./broca_vcf PindelVCF/$sampleID.vcf PindelVCFBROCA/$sampleID.vcf
