import sys, sqlite3

INPUT = sys.stdin
outFilePath = sys.argv[2]

outFile = open(outFilePath, "w")

contigs = []
for line in INPUT:
    if line.startswith("@"):
        outFile.write(line)

        if line.startswith("@SQ"):
            contigs.append(line.split("\t")[1].replace("SN:", ""))
    else:
        break

db = sqlite3.connect(':memory:')
db.isolation_level = None

cursor = db.cursor()
cursor.execute('PRAGMA synchronous=OFF')
cursor.execute('PRAGMA count_changes=OFF')
cursor.execute('PRAGMA journal_mode=MEMORY')
cursor.execute('PRAGMA temp_store=MEMORY')
db.commit()

print "Creating tables"
for contig in contigs:
    cursor.execute("CREATE TABLE reads" + "_" + contig.replace(".", "_") + "(position INTEGER, read TEXT)")
db.commit()

cursor.execute('BEGIN TRANSACTION')

## Output line that we parsed above from file
## Try executemany function

count = 0
chunkCount = 0
for line in INPUT:
    lineItems = line.split("\t")
    rname = lineItems[2]
    pos = int(lineItems[3])

    cursor.execute("INSERT INTO reads" + "_" + rname.replace(".", "_").replace("*", "__star__") + "(position, read) VALUES(?,?)", (pos, line))

    count += 1
    if count == 1000000:
        chunkCount += 1
        print "Finished chunk %i" % chunkCount
        cursor.execute('COMMIT')
        cursor.execute('BEGIN TRANSACTION')
        count = 0

cursor.execute('COMMIT')

print "Calling select statement"
cursor.execute('SELECT read FROM reads ORDER BY chromosome, position')
for row in cursor:
    outFile.write(row[0])

db.close()
outFile.close()
