import os, sys

inFilePath = sys.argv[1] # From Picard

inFile = open(inFilePath)

dataInNextLine = False

for line in inFile:
    if line.startswith("MEDIAN_INSERT_SIZE"):
        headerItems = line.rstrip().split("\t")
        dataInNextLine = True
        continue

    if dataInNextLine:
        dataLineItems = line.rstrip().split("\t")
        print "%i" % float(dataLineItems[headerItems.index("MEAN_INSERT_SIZE")])
        break
