import sys, sqlite3
import pysam
from blist import sorteddict

#https://pypi.python.org/pypi/blist/?

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

inFile = pysam.AlignmentFile(inFilePath, "rb")

contigs = [sqDict["SN"] for sqDict in inFile.header["SQ"]]

contigDict = sorteddict({})
for contig in contigs:
    contigDict[contig] = sorteddict({})

print "starting fetch"
count = 0
for row in inFile.fetch():
    contigDict[inFile.getrname(row.reference_id)][row.reference_start] = row

    count += 1
    if count % 1000000 == 0:
        print count

#print "starting save to output file"
#outFile = pysam.AlignmentFile(outFilePath, "wb", template=inFile)
#for contig in contigs:
#    for position in contigDict[contig]:
#        outFile.write(str(contigDict[contig][position]))
#outFile.close()


#count = 0
#for contig in contigs:
#    contigDict = sorteddict({})
#    for row in inFile.fetch(contig):
#        contigDict[row.reference_start] = row
#        count += 1
#
#    outFile = pysam.AlignmentFile(outFilePath, "wb", template=inFile)
#    for key in contigDict:
#        outFile.write(contigDict[key])
#    outFile.close()
#    break
