import os, sys, fileinput

for line in fileinput.input():
    if not line.startswith("@"):
        exit(0)

    if line.startswith("@RG"):
        print line
