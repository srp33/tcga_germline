###################
# Does not work, needs more work, maybe obsolete
###################
import os, sys, glob

outFilePath = sys.argv[1]
inFilePaths = sys.argv[2:]

outFile = open(outFilePath, 'w')

# Write meta header lines
for line in file(inFilePaths[0]):
    if line.startswith("##"):
        outFile.write(line)
    else:
        break

# Get data header values
headerItems = [line.rstrip().split("\t")[:9] for line in file(inFilePaths[0]) if line.startswith("#") and not line.startswith("##")][0]

# Get sample IDs
sampleIDs = []
for inFilePath in inFilePaths:
    sampleIDs += [line.rstrip().split("\t")[9:] for line in file(inFilePath) if line.startswith("#") and not line.startswith("##")][0]

# Write data header line
outFile.write("\t".join(headerItems + sampleIDs) + "\n")

variantDict = {}

for inFilePath in inFilePaths:
    for line in file(inFilePath):
        if line.startswith("#"):
            continue



outFile.close()
