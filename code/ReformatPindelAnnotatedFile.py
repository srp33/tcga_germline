# Currently, this is designed only for Pindel files that contain data for one sample.
import os, sys

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

inFile = open(inFilePath)
outFile = open(outFilePath, 'w')

headerItems = inFile.readline().rstrip().split("\t")

variantInfoIndices = [i for i in range(len(headerItems)) if headerItems[i] != "info" and not headerItems[i].startswith("gts.") and not headerItems[i].startswith("gt_ref_depths.") and not headerItems[i].startswith("gt_alt_depths.") and not headerItems[i] == "variant_id"]
genotypeIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gts.")]
refDepthIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gt_ref_depths.")]
altDepthIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gt_alt_depths.")]

headerItems = [x.replace("gts.", "") for x in headerItems]
sampleIDs = [headerItems[i] for i in genotypeIndices]

chromIndex = headerItems.index("chrom")
startIndex = headerItems.index("start")
endIndex = headerItems.index("end")
typeIndex = headerItems.index("type")
subTypeIndex = headerItems.index("sub_type")

outFile.write("\t".join([headerItems[i] for i in variantInfoIndices] + sampleIDs) + "\n")

lineCount = 0
for line in inFile:
    lineCount += 1
    if lineCount % 10000 == 0:
        print lineCount

    lineItems = line.rstrip().split("\t")

    genotypeDict = {}
    for i in genotypeIndices:
        genotypeDict[headerItems[i]] = lineItems[i]

    refDepthDict = {}
    for i in refDepthIndices:
        refDepthDict[headerItems[i].replace("gt_ref_depths.", "")] = lineItems[i]

    altDepthDict = {}
    for i in altDepthIndices:
        altDepthDict[headerItems[i].replace("gt_alt_depths.", "")] = lineItems[i]

    refCountItems = []
    for sampleID in sampleIDs:
        ref = genotypeDict[sampleID].split("/")[0]
        alt = genotypeDict[sampleID].split("/")[1]
        refDepth = int(refDepthDict[sampleID])
        altDepth = int(altDepthDict[sampleID])

        #if ref == "." or alt == "." or ref == alt or altDepth == 0:
        if ref == "." or alt == "." or altDepth == 0:
            refCountItems.append("-")
        else:
            refCountItems.append("%i | %i" % (refDepth, altDepth))

    outFile.write("\t".join([lineItems[i] for i in variantInfoIndices] + refCountItems) + "\n")

outFile.close()
inFile.close()
