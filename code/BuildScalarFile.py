import os, sys, glob
from utilities import *

inFilePattern = sys.argv[1]
outFilePath = sys.argv[2]
outColumnHeader = sys.argv[3]

outFile = open(outFilePath, 'w')
outFile.write("\t%s\n" % outColumnHeader)

for inFilePath in glob.glob(inFilePattern):
    inFileName = os.path.basename(inFilePath)
    scalarValue = readScalarFromFile(inFilePath)

    outFile.write("%s\t%s\n" % (inFileName, scalarValue))

outFile.close()
