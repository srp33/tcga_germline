import os, sys

inFilePath = sys.argv[1]

lines = [line.rstrip() for line in file(inFilePath)]

for i in range(1, len(lines)):
    lines[i] = " ".join(lines[i].split(" ")[1:])

outText = " , ".join(lines)

outFile = open(inFilePath, "w")
outFile.write(outText)
outFile.close()
