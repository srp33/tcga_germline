import os, sys, glob, gzip

inFilePattern = sys.argv[1]
outFilePath = sys.argv[2]

outHeaderItems = []
sampleIDs = []
outCountDict = {}
outVariantInfoDict = {}
outQualDict = {}
outFilterDict = {}
outDepthDict = {}

for inFilePath in sorted(glob.glob(inFilePattern)):
    sampleID = os.path.basename(inFilePath).replace(".gz", "")
    print inFilePath

    inFile = gzip.open(inFilePath)
    headerItems = inFile.readline().rstrip().split("\t")

    if len(outCountDict) == 0:
        outHeaderItems = headerItems[:-1]

        chromosomeIndex = outHeaderItems.index("chrom")
        startIndex = outHeaderItems.index("start")
        endIndex = outHeaderItems.index("end")
        refIndex = outHeaderItems.index("ref")
        altIndex = outHeaderItems.index("alt")
        qualIndex = outHeaderItems.index("qual")
        filterIndex = outHeaderItems.index("filter")
        depthIndex = outHeaderItems.index("depth")

    sampleIDs.append(sampleID)

    for line in inFile:
        lineItems = line.rstrip().split("\t")
        chromosome = lineItems[chromosomeIndex]
        start = lineItems[startIndex]
        end = lineItems[endIndex]
        ref = lineItems[refIndex]
        alt = lineItems[altIndex]
        qual = lineItems[qualIndex]
        filt = lineItems[filterIndex]
        depth = lineItems[depthIndex]
        counts = lineItems[-1]
        variantID = tuple([chromosome, start, end, ref, alt])

        if variantID not in outVariantInfoDict:
            outVariantInfoDict[variantID] = lineItems[:-1]

        outQualDict[variantID] = outQualDict.setdefault(variantID, []) + [qual]
        outFilterDict[variantID] = outFilterDict.setdefault(variantID, []) + [filt]
        outDepthDict[variantID] = outDepthDict.setdefault(variantID, []) + [depth]

        if variantID not in outCountDict:
            outCountDict[variantID] = {}

        outCountDict[variantID][sampleID] = counts

    inFile.close()

#Remove any rows that have all missing values
for key in outCountDict.keys():
    if len([x for x in outCountDict[key] if x != "-"]) == 0:
        del outCountDict[key]

print "Saving output to %s" % outFilePath
outFile = open(outFilePath, 'w')
outFile.write("\t".join(outHeaderItems + sampleIDs) + "\n")
for variantID in sorted(outCountDict.keys()):
    outItems = list(outVariantInfoDict[variantID])

    outItems[qualIndex] = ",".join(outQualDict[variantID])
    outItems[filterIndex] = ",".join(outFilterDict[variantID])
    outItems[depthIndex] = ",".join(outDepthDict[variantID])

    for sampleID in sampleIDs:
        if sampleID in outCountDict[variantID]:
            outItems.append(outCountDict[variantID][sampleID])
        else:
            outItems.append("-")

    outFile.write("\t".join(outItems) + "\n")

outFile.close()
