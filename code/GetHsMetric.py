import os, sys

inFilePath = sys.argv[1] # From Picard
metric = sys.argv[2]

inFile = open(inFilePath)

dataInNextLine = False

for line in inFile:
    if line.startswith("BAIT_SET"):
        headerItems = line.rstrip().split("\t")
        dataInNextLine = True
        continue

    if dataInNextLine:
        dataLineItems = line.rstrip().split("\t")
        print "%s" % dataLineItems[headerItems.index(metric)]
        break
