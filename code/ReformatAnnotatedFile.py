import os, sys

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

inFile = open(inFilePath)
outFile = open(outFilePath, 'w')

headerItems = inFile.readline().rstrip().split("\t")

variantInfoIndices = [i for i in range(len(headerItems)) if headerItems[i] != "info" and not headerItems[i].startswith("gts.") and not headerItems[i].startswith("gt_ref_depths.") and not headerItems[i].startswith("gt_alt_depths.") and not headerItems[i] == "variant_id"]
genotypeIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gts.")]
refDepthIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gt_ref_depths.")]
altDepthIndices = [i for i in range(len(headerItems)) if headerItems[i].startswith("gt_alt_depths.")]

headerItems = [x.replace("gts.", "") for x in headerItems]
#sampleIDs = sorted([headerItems[i] for i in genotypeIndices])
sampleIDs = [headerItems[i] for i in genotypeIndices]

chromIndex = headerItems.index("chrom")
startIndex = headerItems.index("start")
endIndex = headerItems.index("end")
typeIndex = headerItems.index("type")
subTypeIndex = headerItems.index("sub_type")

outFile.write("\t".join([headerItems[i] for i in variantInfoIndices] + sampleIDs) + "\n")

numMissing = 0
totalValues = 0
lineCount = 0
for line in inFile:
    lineCount += 1
    if lineCount % 10000 == 0:
        print lineCount

    lineItems = line.rstrip().split("\t")
    #variantID = "%s__%s__%s__%s__%s" % (lineItems[chromIndex], lineItems[startIndex], lineItems[endIndex], lineItems[typeIndex], lineItems[subTypeIndex])

    genotypeDict = {}
    for i in genotypeIndices:
        genotypeDict[headerItems[i]] = lineItems[i]

    refDepthDict = {}
    for i in refDepthIndices:
        refDepthDict[headerItems[i].replace("gt_ref_depths.", "")] = lineItems[i]

    altDepthDict = {}
    for i in altDepthIndices:
        altDepthDict[headerItems[i].replace("gt_alt_depths.", "")] = lineItems[i]

    refCountItems = []
    for sampleID in sampleIDs:
        ref = genotypeDict[sampleID].split("/")[0]
        alt = genotypeDict[sampleID].split("/")[1]
        refDepth = int(refDepthDict[sampleID])
        altDepth = int(altDepthDict[sampleID])

        totalValues += 1

        if ref == "." or alt == "." or altDepth == 0:
            refCountItems.append("-")
            print lineItems
            numMissing += 1
        else:
            refCountItems.append("%i | %i" % (refDepth, altDepth))

    outFile.write("\t".join([lineItems[i] for i in variantInfoIndices] + refCountItems) + "\n")

outFile.close()
inFile.close()

print "%i / %i genotypes were marked as missing in %s." % (numMissing, totalValues, inFilePath)
