import sys
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

outHeaderFilePath = sys.argv[2]

INPUT = sys.stdin

outHeaderFile = open(outHeaderFilePath, "w")

for line in INPUT:
    if line.startswith("@"):
        outHeaderFile.write(line)
    else:
        sys.stdout.write(line)
        break

for line in INPUT:
    sys.stdout.write(line)
    sys.stdout.flush()

outHeaderFile.close()
