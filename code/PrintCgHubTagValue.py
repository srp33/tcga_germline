import os, sys, glob

inFilePath = sys.argv[1]
tagName = sys.argv[2]

for line in file(inFilePath):
    if line.strip().startswith("<%s>" % tagName):
        print line.strip().replace("/", "").replace("<%s>" % tagName, "")
        exit(0)
