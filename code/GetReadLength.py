#! /usr/bin/env python
import sys
import re
import pysam

bamFilePath = sys.argv[1]

bamFile = pysam.AlignmentFile(bamFilePath, "rb")

readLengths = set()
lengthsDict = {}

count = 0
for read in bamFile.fetch():
    readLengths.add(read.query_length)

    lengthsDict[read.query_length] = lengthsDict.setdefault(read.query_length, 0) + 1

    count += 1
    if count > 100000:
        break

bamFile.close()

maxFrequency = max(lengthsDict.values())
primaryReadLength = [x for x,y in lengthsDict.items() if y ==maxFrequency][0]

print primaryReadLength
